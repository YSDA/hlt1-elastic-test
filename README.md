## How to run the experiment
1. Get HLT1 data, for example `/afs/cern.ch/user/n/nkazeev/public/166433.json.gz`
2. Run the Elastic container `docker run --name elastic -d elasticsearch`
4. Clone the repository `git clone https://gitlab.cern.ch/YSDA/hlt1-elastic-test/tree/master`
3. Find the elastic IP with `docker inspect elastic`
5. Set the correct Elastic IP in index_tree.py
6. Edit the CMD in Dockerfile to point to your data
7. Build the Docker image `docker build -t elastic-feeder hlt1-elastic-test`
8. Run the Docker image `docker run -v <folder with 166433.json.gz>:/data -it --link elastic elastic-feeder`
