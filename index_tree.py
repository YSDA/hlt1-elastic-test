#!/usr/bin/env python3
from elasticsearch import Elasticsearch
import elasticsearch.helpers
import argparse
import gzip
import ujson
import datetime
import io


INDEX_NAME = 'hlt1'
ELASTIC_HOST = "172.17.0.2:9200"

def sanitize_event(event):
    """Replaces . with _ in field names"""
    return dict(map(lambda key_value: (key_value[0].replace(".", "_"), key_value[1]), event.items()))

# Based on an old pilot https://github.yandex-team.ru/anaderi/cern/blob/dev/junk/kazeevn/elastic_search_index.py
# TODO Aviod index/type repetion
# http://www.elasticsearch.org/guide/en/elasticsearch/guide/current/bulk.html
def get_action(event):
    return {
        '_index': INDEX_NAME,
        '_type': 'event',
        '_source': ujson.loads(event)
        }


MAPPING = {
    "mappings": {
        "event": {
            "_all": {"enabled": False},
            "_source": {"enabled": True},
            "dynamic": False, 
            "properties": {
                "gpsTime": {"type": "long"}
            }
        }
    }
}

def recreate_index(es):
    es.indices.delete(index=INDEX_NAME, ignore=(404, ))
    es.indices.create(index=INDEX_NAME, body=MAPPING)

def main():
    parser = argparse.ArgumentParser("Index Eventindex data into elasticsearch")
    parser.add_argument("input_file", type=str,
                        help=".json.gz file with events to import")
    args = parser.parse_args()
    es = Elasticsearch(ELASTIC_HOST)
    recreate_index(es)
    started_bulk = datetime.datetime.now()
    print("Starting bulk indexing at %s" % str(started_bulk))
    with gzip.open(args.input_file, "rb") as input_file:
        bulk_result = elasticsearch.helpers.bulk(client=es,
            actions=map(get_action, io.BufferedReader(input_file)))
    ended_bulk = datetime.datetime.now()
    bulk_time = (ended_bulk - started_bulk).total_seconds()
    total_indexed = es.count(index=INDEX_NAME, doc_type="event")["count"]
    bulk_speed = total_indexed/bulk_time
    print("Ended bulk, %s, took %s, speed %f Hz" % (str(ended_bulk), str(bulk_time), bulk_speed))
    #TODO(kazeevn) add elasticsearch .flush and .optimize

if __name__ == '__main__':
    main()
