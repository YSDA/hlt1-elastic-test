FROM continuumio/miniconda3
RUN conda install conda-build
RUN conda skeleton pypi urllib3
RUN conda build urllib3/
RUN conda install urllib3 --use-local
RUN conda skeleton pypi pyaml nosexcover
RUN conda build pyaml nosexcover
RUN conda install pyaml nosexcover --use-local
RUN conda skeleton pypi elasticsearch
RUN conda build elasticsearch/
RUN conda install elasticsearch --use-local
RUN conda install ipython
RUN conda install ujson

ADD index_tree.py /root/index_tree.py
CMD python /root/index_tree.py /data/166433.json.gz 